/**
 * Naive partition function to allow for bucketing of objects based on a
 * specified numeric span. Only paritions by the 'date' field in supplied item
 * set.
 */
export function partitionBySpan(span, items) {
  let partitions = items.reduce((memo, item) => {
    let currTime = item.date

    if (memo.range[0] == null) {
      memo.range = [currTime, currTime + span];
      memo.currentSet.push(item);
    } else if (memo.range[1] < currTime) {
      memo.final.push([memo.range, memo.currentSet]);
      memo.range = [currTime, currTime + span];
      memo.currentSet = [item];
    } else {
      memo.currentSet.push(item);
    }

    return memo;
  }, {
    range: [null, null],
    currentSet: [],
    final: []
  })

  let final = partitions.final;
  final.push([partitions.range, partitions.currentSet]);
  return final;
}
