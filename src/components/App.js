import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import * as selectors from '../selectors';

import AggregrateModeSelector from './AggregrateModeSelector';
import LocationSelector from './LocationSelector';
import UnitSelector from './UnitSelector';
import WeatherGraph from './WeatherGraph';

const App = ({graphOpacity}) =>
  <div>
    <header>
      <div style={{float: 'left'}}>
        <LocationSelector />
      </div>
      <div style={{float: 'left'}}>
        <UnitSelector />
      </div>
      <AggregrateModeSelector />
    </header>

    <div style={{opacity: graphOpacity}}>
      <WeatherGraph />
    </div>
  </div>

App.propTypes = {
  graphOpacity: PropTypes.number
}

const mapStateToProps = state => ({
  graphOpacity: selectors.getFetchStatus(state) == 'fetching' ? 0.2 : 1
})

export default connect(mapStateToProps)(App);
