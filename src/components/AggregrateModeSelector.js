import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '../actions';
import * as selectors from '../selectors';

const AggregrateModeSelector = ({mode, updateAggregrateMode}) =>
  <div>
    <select value={mode} onChange={updateAggregrateMode}>
      <option value='day'>Day</option>
      <option value='week'>Week</option>
    </select>
  </div>

AggregrateModeSelector.propTypes = {
  mode: PropTypes.string,
  updateAggregrateMode: PropTypes.func
}

const mapDispatchToProps = dispatch => ({
  updateAggregrateMode: evt => dispatch(actions.updateAggregrateMode(evt.target.value)),
});

const mapStateToProps = state => ({
  mode: selectors.getAggregateMode(state),
})

export default connect(mapStateToProps, mapDispatchToProps)(AggregrateModeSelector);
