import React from 'react';
import { connect } from 'react-redux';
import * as d3 from 'd3';
import * as selectors from '../selectors';

class WeatherGraph extends React.Component {
  constructor(props) {
    super(props);
    this.svgRef = React.createRef();
  }

  buildBase() {
    this.xScale = d3.scaleTime().range([0, 900]);
    this.yScale = d3.scaleLinear().range([300, 0]);
    this.svg = d3.select(this.svgRef.current)
      .append('g')
      .attr("transform", "translate(30, 10)")

    this.lineGroup = this.svg.append("g")
      .attr("transform", "translate(30, 40)")

    this.yAxis = this.svg.append("g")
      .attr("transform", "translate(0, 40)")

    this.highLine = d3.line()
      .x(d => this.xScale(d.date))
      .y(d => this.yScale(d.max));

    this.lowLine = d3.line()
      .x(d => this.xScale(d.date))
      .y(d => this.yScale(d.min));

  }

  buildLine(data, klass, lineFn, renderFn) {
    let lines = this.lineGroup.selectAll(`.${klass}`).data([data])

    lines.transition().duration(500).attr('d', lineFn);

    lines.enter()
      .append('path')
      .attr('class', klass)
      .attr('fill', 'none')
      .style('stroke-width', '2px')
      .call(renderFn)

    lines.exit().remove();
  }

  buildOverlay(data, klass, accessor, circleFn) {
    let overlayGroup = this.lineGroup.selectAll(`.${klass}`).data(data)

    let og = overlayGroup.enter()
      .append('g')
      .attr('class', klass)
      .attr("transform", d =>
        `translate(${this.xScale(d.date)}, ${this.yScale(accessor(d))})`
      )

    overlayGroup.transition()
      .duration(500)
      .attr("transform", d =>
        `translate(${this.xScale(d.date)}, ${this.yScale(accessor(d))})`
      )
      .select('text')
      .tween('text', function (d) {
        let sel = d3.select(this);
        let iFn = d3.interpolate(parseFloat(sel.text()), accessor(d));
        let fmt = d3.format(".1f");
        return t => sel.text(`${fmt(iFn(t))}°`)
      })

    og.append('circle')
      .call(circleFn)
      .attr("r", 6);

    og.append('text')
      .text(d => `${accessor(d)}°`)
      .attr("text-anchor", "middle")
      .attr("transform", "translate(3, -15)")

    overlayGroup.exit().remove();
  }


  updateGraph() {
    const data = this.props.computedGraphData;

    this.xScale.domain(d3.extent(data, d => d.date));
    this.yScale.domain(d3.extent(data.flatMap(day => [day.min, day.max])));

    let dates = this.lineGroup.selectAll('.dates')
      .data(data)

    dates
      .enter()
      .append('text')
      .attr('class', 'dates')
      .text(d => d.displayDate)
      .attr("text-anchor", "middle")
      .attr("transform", d => `translate(${this.xScale(d.date)}, -30)`);

    dates
      .text(d => d.displayDate)

    dates.exit().remove();

    this.buildLine(data, 'low-line',  this.lowLine,  e => e.style('stroke', 'blue'));
    this.buildLine(data, 'high-line', this.highLine, e => e.style('stroke', 'red'));

    this.buildOverlay(data, 'high-overlay', e => e.max, e => e.attr('fill', 'red'))
    this.buildOverlay(data, 'low-overlay', e => e.min, e => e.attr('fill', 'blue'))

    this.yAxis.transition()
      .duration(500)
      .call(d3.axisLeft(this.yScale))
  }

  componentDidMount() {
    this.buildBase();
    this.updateGraph();
  }

  componentDidUpdate() {
    if (this.props.fetchStatus == 'not-fetching') this.updateGraph();
  }

  render() {
    return(
      <svg
        width='980'
        height='400'
        ref={this.svgRef} />
    )
  }
}

const mapStateToProps = state => ({
  computedGraphData: selectors.getComputedGraphData(state),
  fetchStatus:       selectors.getFetchStatus(state)
})

export default connect(mapStateToProps)(WeatherGraph);
