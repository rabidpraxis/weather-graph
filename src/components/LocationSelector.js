import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '../actions';
import * as selectors from '../selectors';

const LocationSelector = ({locations, selected, updateLocation}) =>
  <div>
    <select value={selected} onChange={updateLocation}>
      {locations.map(location =>
        <option key={location} value={location}>{location}</option>
      )}
    </select>
  </div>

LocationSelector.propTypes = {
  locations: PropTypes.array,
  selected: PropTypes.string,
  updateLocation: PropTypes.func
}

const mapDispatchToProps = dispatch => ({
  updateLocation: evt => dispatch(actions.updateLocation(evt.target.value)),
});

const mapStateToProps = state => ({
  locations: selectors.getAllLocations(state),
  selected:  selectors.getLocation(state)
})

export default connect(mapStateToProps, mapDispatchToProps)(LocationSelector);
