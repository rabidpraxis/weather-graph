import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '../actions';
import * as selectors from '../selectors';

const UnitSelector = ({unit, updateUnit}) =>
  <select value={unit} onChange={updateUnit}>
    <option value='f'>Fahrenheit (f)</option>
    <option value='c'>Celsius (c)</option>
  </select>

UnitSelector.propTypes = {
  unit: PropTypes.string,
  updateUnit: PropTypes.func
}

const mapDispatchToProps = dispatch => ({
  updateUnit: evt => dispatch(actions.updateUnit(evt.target.value)),
});

const mapStateToProps = state => ({
  unit: selectors.getUnit(state),
})

export default connect(mapStateToProps, mapDispatchToProps)(UnitSelector);
