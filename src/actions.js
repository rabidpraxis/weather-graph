/**
 * Types
 */
let types = {}
const createActionType = name => types[name] = name;

createActionType('LOCATION_FORECASTS_MERGE');
createActionType('LOCATION_UPDATE');
createActionType('UNIT_UPDATE');
createActionType('FETCH_STATUS_UPDATE');
createActionType('AGGREGRATE_MODE_UPDATE');

/**
 * Actions
 */

/**
 * Simple helper to create actions that can specify an optional payload, for
 * trigger type actions
 *
 * @returns {function}
 */
function buildAction(type, payload = true) {
  if (payload) {
    return aPayload => ({ type: types[type], payload: aPayload });
  }
  return () => ({ type: actionTypes[type] });
}

export const mergeLocationForecasts = buildAction(types.LOCATION_FORECASTS_MERGE);
export const updateLocation = buildAction(types.LOCATION_UPDATE);
export const updateUnit = buildAction(types.UNIT_UPDATE);
export const updateFetchStatus = buildAction(types.FETCH_STATUS_UPDATE);
export const updateAggregrateMode = buildAction(types.AGGREGRATE_MODE_UPDATE);

export { types }
