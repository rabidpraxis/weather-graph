import fetch from 'isomorphic-fetch';
import moment from 'moment';
import { range } from 'lodash';

/**
 * Default cache, utilizing localStorage, interface modeled after Map implementation
 */
const localStorageCache = {
  has: key => localStorage.getItem(key),
  get: key => JSON.parse(localStorage.getItem(key)),
  set: (k, v) => localStorage.setItem(k, JSON.stringify(v))
}

const getWeatherKey = (location, date) => `${location}-${date}`;
const getWeatherURL = (location, date) =>
  `https://api.apixu.com/v1/history.json?key=${process.env.APIXU_KEY}&q=${location}&dt=${date}`;

const defaultContext = {
  fetch,
  cache: localStorageCache
}

const extractForecast = (data) => data.forecast.forecastday[0];

export function fetchWeatherByLocationAndDate(location, date, context = defaultContext) {
  const cacheKey = getWeatherKey(location, date);

  if(context.cache.has(cacheKey)) {
    return Promise.resolve(context.cache.get(cacheKey));
  } else {
    return context.fetch(getWeatherURL(location, date))
      .then(response => response.json())
      .then(json => {
        const forecast = extractForecast(json);
        context.cache.set(cacheKey, forecast);
        return forecast;
      })
  }
}

export function fetchWeekWeatherByLocation(location, context = defaultContext) {
  const weekRange = range(6, -1).map(i => moment().subtract(i, 'days').format('YYYY-MM-DD'))

  return Promise.all(
    weekRange.map(date => fetchWeatherByLocationAndDate(location, date, context))
  )
}
