import { types } from './actions';
import { merge, update } from 'lodash/fp';

const initialState = {
  aggregateMode: 'day',
  allLocations: [
    'San Francisco, CA',
    'Amsterdam, Netherlands',
    'Oakland, CA',
    'Rome, Italy',
    'Cleveland, OH',
    'Tel Aviv, Israel',
    'New York City, NY',
    'Murmansk, Russia',
    'Istanbul, Turkey',
  ],
  location: 'Amsterdam, Netherlands',
  fetchStatus: 'not-fetching',
  weatherData: {},
  unit: 'f'
}

export default function reducers(state = initialState, { type, payload } = {}) {
  switch (type) {
    case types.AGGREGRATE_MODE_UPDATE:
      return { ...state, aggregateMode: payload }
    case types.FETCH_STATUS_UPDATE:
      return { ...state, fetchStatus: payload }
    case types.UNIT_UPDATE:
      return { ...state, unit: payload }
    case types.LOCATION_UPDATE:
      return { ...state, location: payload }
    case types.LOCATION_FORECASTS_MERGE:
      const { forecasts, location } = payload;
      const mergeData = forecasts.reduce((memo, forecast) => {
        memo[forecast.date_epoch] = forecast
        return memo
      }, {})

      return update(`weatherData.${location}`, loc => merge(loc, mergeData), state)
    default:
      return state
  }
}
