import moment from 'moment';
import { partitionBySpan } from './utils';

export const getAllLocations = state => state.allLocations;
export const getLocation = state => state.location;
export const getWeatherData = state => state.weatherData;
export const getUnit = state => state.unit;
export const getFetchStatus = state => state.fetchStatus;
export const getAggregateMode = state => state.aggregateMode;

/**
 * Selector to combine all states and produce the right data for our dumb
 * WeatherGraph chart
 */
export const getComputedGraphData = state => {
  const location = getLocation(state);
  const data     = getWeatherData(state);
  const unit     = getUnit(state);
  const mode     = getAggregateMode(state);
  const locData  = data[location];

  if (!locData) return []

  const formatEpoch = date => moment.unix(date).utc().format('M/D');
  const avgDayTemp = (days, accessor) =>
    days.reduce((sum, d) => d[accessor] + sum, 0) / days.length;

  const extractedDays = Object.values(locData).map(({day, date_epoch}) => ({
    date: date_epoch,
    min: day[`mintemp_${unit}`],
    max: day[`maxtemp_${unit}`]
  }))

  switch (mode) {
    case 'day':
      return extractedDays.map(day => {
        day.displayDate = formatEpoch(day.date)
        return day;
      });
    case 'week':
      let weeks = partitionBySpan(604800, extractedDays)
      return weeks.map(([[start, end], days]) => ({
        date: start,
        displayDate: `${formatEpoch(start)} - ${formatEpoch(end)}`,
        min: avgDayTemp(days, 'min'),
        max: avgDayTemp(days, 'max')
      }))
  }
}
