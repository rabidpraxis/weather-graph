import { select, put, call, takeLatest, all } from 'redux-saga/effects';
import * as api from './api';
import * as selectors from '../src/selectors';
import * as actions from './actions';

export function* fetchWeather(API) {
  try {
    yield put(actions.updateFetchStatus('fetching'));
    const location = yield(select(selectors.getLocation));
    const forecasts = yield call(API.fetchWeekWeatherByLocation, location);
    yield put(actions.mergeLocationForecasts({forecasts, location}));
  } catch (err) {
    console.error(err);
  }

  yield put(actions.updateFetchStatus('not-fetching'));
}

export function* watchActions() {
  yield takeLatest(actions.types.LOCATION_UPDATE, fetchWeather, api)
}

export default function* sagas() {
  yield all([
    watchActions(),
    call(fetchWeather, api)
  ])
}
