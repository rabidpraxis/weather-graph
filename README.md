# Weather Graph

### Bootstrap

Built with node `9.3.0`. It may not be compatible with any other version.

After clone, make sure to get dependencies:

```shell
npm i
```

### Running

The simplest method to view the project is through the `webpack-dev-server`. There is a helper npm script.

#### Specifing API Key

The APIXU key must be supplied as an environment variable when running the server. Here is what the command should look like:

```shell
APIXU_KEY=api_key npm run dev
```

Once everything loads, you should get a localhost url output in the console. Visit and enjoy!

### Tests

You can run tests with:

```shell
npm run test
```

### How?

Take a look at this gorgeous wireframe:

![wired](doc/wireframe.jpg)

I got close.

### A note about caching

Weather data is cached to localStorage. If you want to retry making requests, then use the "Application" tab in Chrome's dev tools to clear Local Storage.