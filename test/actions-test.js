import { expect } from 'chai';

import * as actions from '../src/actions';

const containsAction = (name, type) => {
  it(`contains ${name}`, function() {
    expect(actions[name]({a: 'b'})).to.eql({type, payload: {a: 'b'}})
  });
}

describe('Actions', function() {
  describe('actions', function() {
    containsAction('mergeLocationForecasts', 'LOCATION_FORECASTS_MERGE');
    containsAction('updateLocation', 'LOCATION_UPDATE');
    containsAction('updateUnit', 'UNIT_UPDATE');
    containsAction('updateFetchStatus', 'FETCH_STATUS_UPDATE');
    containsAction('updateAggregrateMode', 'AGGREGRATE_MODE_UPDATE');
  });
});
