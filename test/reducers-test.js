import reducers from '../src/reducers';
import { types } from '../src/actions';
import { expect } from 'chai';

const buildAction = (type, payload) => ({type, payload})

const updateStateWith = (type, prop) => {
  it(`updates ${prop} via ${type}`, function() {
    const action = buildAction(type, 'updated');
    expect(reducers({}, action)).to.eql({[prop]: 'updated'});
  });
}

describe('Reducers', function() {
  it('passes through initial data', function() {
    expect(reducers({a: 2})).to.eql({a: 2})
  });

  describe('Basic Reducers', function() {
    updateStateWith(types.LOCATION_UPDATE, 'location');
    updateStateWith(types.UNIT_UPDATE, 'unit');
    updateStateWith(types.FETCH_STATUS_UPDATE, 'fetchStatus');
    updateStateWith(types.AGGREGRATE_MODE_UPDATE, 'aggregateMode');
  });

  describe('LOCATION_FORECASTS_MERGE', function() {
    it('indexes & merges into weatherData based on epoch time', function() {
      const action = buildAction(types.LOCATION_FORECASTS_MERGE, {
        location: 'Test Location',
        forecasts: [ { date_epoch: 20000 } ]
      })

      expect(reducers({weatherData: {}}, action)).to.eql({
        weatherData: {
          'Test Location': { 20000: { date_epoch: 20000 } }
        }
      });
    });

    it('merges into current data', function() {
      const action = buildAction(types.LOCATION_FORECASTS_MERGE, {
        location: 'Location 2',
        forecasts: [{ date_epoch: 99 }]
      })

      const base = {
        'Location 2': {
          100: { date_epoch: 100 }
        },
        'Location 1': {
          500: { date_epoch: 500 }
        }
      }

      expect(reducers({weatherData: base}, action)).to.eql({
        weatherData: {
          'Location 1': {
            500: { date_epoch: 500 }
          },
          'Location 2': {
            100: { date_epoch: 100 },
            99: { date_epoch: 99 }
          }
        }
      });
    });
  });
});

