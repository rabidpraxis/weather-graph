import { expect } from 'chai';
import sinon from 'sinon';

import * as api from '../src/api';

const testWeatherCache = {
  has: k => false,
  get: k => 'nope',
  set: (k, v) => 12
}

const buildFetch = response => url => Promise.resolve({json: () => response})

const testContext = {
  fetch: buildFetch({}),
  cache: testWeatherCache
}

// Mock API key for tests
process.env.APIXU_KEY = 'test';

describe('Api', function() {
  describe('fetchWeatherByLocationAndDate', function() {
    it('makes a fetch request and caches result', async function() {
      const fetchFake = sinon.fake.resolves({json: () => ({forecast: {forecastday: ['forecastData']}})});
      const cacheSetFake = sinon.fake();
      const cacheFake = Object.assign({}, testWeatherCache, {set: cacheSetFake});

      let ret = await api.fetchWeatherByLocationAndDate('test', '2018-01-10', {
        cache: cacheFake,
        fetch: fetchFake
      })

      expect(cacheSetFake.args[0]).to.eql(['test-2018-01-10', 'forecastData']);
      expect(fetchFake.lastArg).to.eql('https://api.apixu.com/v1/history.json?key=test&q=test&dt=2018-01-10');
      expect(ret).to.eql('forecastData');
    });

    it('does not fetch if location is cached', async function() {
      const cacheFake = Object.assign({}, testWeatherCache, {
        has: _ => true,
        get: key => 'bool'
      });

      const fetchFake = sinon.fake();

      let ret = await api.fetchWeatherByLocationAndDate('test', '2018-01-10', {
        cache: cacheFake,
        fetch: fetchFake
      });

      expect(ret).to.eql('bool');
      expect(fetchFake.callCount).to.eql(0);
    });
  });

  describe('fetchWeekWeatherByLocation', function() {
    before(() => {
      this.clock = sinon.useFakeTimers({now: 1483228800000});
    })

    after(() => {
      this.clock.restore();
    })

    it('calls fetchWeatherByLocationAndDate for 7 previous days', async function() {
      const cacheSetFake = sinon.fake();
      const cacheFake = Object.assign({}, testWeatherCache, {set: cacheSetFake});

      let ret = await api.fetchWeekWeatherByLocation('test', {
        cache: cacheFake,
        fetch: buildFetch({forecast: {forecastday: ['forcastData']}})
      });

      expect(ret.length).to.eql(7);
      expect(cacheSetFake.args.map(e => e[0])).to.eql([
        'test-2016-12-25',
        'test-2016-12-26',
        'test-2016-12-27',
        'test-2016-12-28',
        'test-2016-12-29',
        'test-2016-12-30',
        'test-2016-12-31'
      ]);
    });
  });
});

