import { expect } from 'chai';
import sinon from 'sinon';
import { select, put, call, takeLatest, all } from 'redux-saga/effects';

import rootSaga, * as sagas from '../src/sagas';
import * as actions from '../src/actions';
import * as selectors from '../src/selectors';
import * as api from '../src/api';

const mockApi = {
  fetchWeekWeatherByLocation: (loc) => {
    return Promise.resolve([])
  }
}

describe('Sagas', function() {
  describe('watchActions', function() {
    it('listens for updated locations', function() {
      const gen = sagas.watchActions();

      expect(gen.next().value).to.eql(
        takeLatest(actions.types.LOCATION_UPDATE, sagas.fetchWeather, api)
      );
    });
  });

  describe('fetchWeather', function() {
    it('calls API and merges results', function() {
      const gen = sagas.fetchWeather(mockApi);
      const location = 'Amsterdam, Netherlands';
      const forecasts = { looks: 'sunny' }

      expect(gen.next().value).to.eql(
        put(actions.updateFetchStatus('fetching'))
      );
      expect(gen.next().value).to.eql(
        select(selectors.getLocation)
      );
      expect(gen.next(location).value).to.eql(
        call(mockApi.fetchWeekWeatherByLocation, location)
      );
      expect(gen.next(forecasts).value).to.eql(
        put(actions.mergeLocationForecasts({ location, forecasts }))
      );
      expect(gen.next().value).to.eql(
        put(actions.updateFetchStatus('not-fetching'))
      );
    });
  });

  describe('sagas', function() {
    it('fetches weather', function() {
      const gen = rootSaga()

      const yielded = all([
        sagas.watchActions(),
        call(sagas.fetchWeather, api)
      ])

      expect(gen.next().value).to.eql(yielded);
    });
  });
});

