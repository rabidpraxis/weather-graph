import { expect } from 'chai';
import * as utils from '../src/utils';

describe('partitionBySpan', function() {
  it('extracts spanned buckets', function() {
    let e = utils.partitionBySpan(100, [{date: 100}, {date: 150}, {date: 250}])
    expect(e).to.eql([
      [[100, 200], [{date: 100}, {date: 150}]],
      [[250, 350], [{date: 250}]]
    ])
  });
});

