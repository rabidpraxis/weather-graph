import { expect } from 'chai';

import * as selectors from '../src/selectors';

const canSelect = (fnName, stateName) => {
  it(`can select ${stateName} with ${fnName}`, function() {
    expect(selectors[fnName]({[stateName]: 'foo'})).to.eql('foo')
  });
}

let buildDay = (date, mintemp_f, maxtemp_f, mintemp_c, maxtemp_c) => ({
  date_epoch: date,
  day: { mintemp_f, maxtemp_f, mintemp_c, maxtemp_c }
})

describe('Selectors', function() {
  describe('Basic Selectors', function() {
    canSelect('getAllLocations', 'allLocations');
    canSelect('getLocation', 'location');
    canSelect('getWeatherData', 'weatherData');
    canSelect('getUnit', 'unit');
    canSelect('getFetchStatus', 'fetchStatus');
    canSelect('getAggregateMode', 'aggregateMode');
  });

  describe('computedGraphData', function() {
    let location = 'Test Location';
    let baseState = () => ({
      location,
      unit: 'f',
      aggregateMode: 'day',
      weatherData: {
        [location]: {
          1535424943: buildDay(1535424943, 100, 120, 10, 15)
        }
      }
    })

    it('deals with empty location data', function() {
      let state = baseState();
      state.weatherData = {};
      expect(selectors.getComputedGraphData(state)).to.eql([])
    });


    describe('day mode', function() {
      it('returns date as seconds from epoch', function() {
        expect(selectors.getComputedGraphData(baseState())[0]).to.contain({ date: 1535424943 })
      });

      it('returns a display date', function() {
        expect(selectors.getComputedGraphData(baseState())[0]).to.contain({ displayDate: '8/28' })
      });

      it('returns Fahrenheit units', function() {
        expect(selectors.getComputedGraphData(baseState())[0]).to.contain({ min: 100, max: 120 })
      });

      it('returns Celsius units', function() {
        let state = baseState();
        state.unit = 'c'

        expect(selectors.getComputedGraphData(state)[0]).to.contain({ min: 10, max: 15 })
      });
    })

    describe('week mode', function() {
      let baseState = () => ({
        location: 'Test Location',
        unit: 'f',
        aggregateMode: 'week',
        weatherData: {
          'Test Location': {
            1534982400: buildDay(1534982400, 1, 2, 10, 20),
            1535068800: buildDay(1535068800, 1, 2, 10, 20),
            1535155200: buildDay(1535155200, 1, 2, 10, 20),
            1535241600: buildDay(1535241600, 1, 2, 10, 20),
            1535328000: buildDay(1535328000, 1, 2, 10, 20),
            1535414400: buildDay(1535414400, 1, 2, 10, 20)
          }
        }
      })

      it('returns a combined display date', function() {
        expect(selectors.getComputedGraphData(baseState())[0]).to.contain({ displayDate: '8/23 - 8/30' })
      });

      it('returns averaged units', function() {
        expect(selectors.getComputedGraphData(baseState())[0]).to.contain({ min: 1, max: 2 })
      })
    });
  })
});

